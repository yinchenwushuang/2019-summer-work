#include<stdio.h>
#include<graphics.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>

#define High 480
#define Width 640
#define PI 3.1415926

int main(void)
{
	initgraph(Width, High);
	
	int center_x = Width / 2;
	int center_y = High / 2;
	int secondlength = Width / 5;
	int minutelength = secondlength - 50;
	int hourlength = minutelength - 20;
	int secondend_x, secondend_y;
	secondend_x = center_x + secondlength;
	secondend_y = center_y;

	int minuteend_x = center_x + minutelength;
	int minuteend_y = center_y;
	int hourEnd_x = center_x + hourlength;
	int hourEnd_y = center_y;

	setcolor(BLUE);
	circle(center_x, center_y, secondlength + 20);
	setfillcolor(GREEN);
	solidcircle(Width / 2 + 4, High / 2 + secondlength + 15, 4);
	TCHAR s_6[5];
	_stprintf(s_6, _T("%d"), 6);
	outtextxy(Width / 2 - 2, High / 2 + secondlength - 5, s_6);
	solidcircle(Width / 2, High / 2 - secondlength - 17, 4);
	TCHAR s_12[5];
	_stprintf(s_12, _T("%d"), 12);
	outtextxy(Width / 2 - 6, High / 2 - secondlength - 13, s_12);
	solidcircle(center_x + secondlength + 17, center_y, 4);
	TCHAR s_3[5];
	_stprintf(s_3, _T("%d"), 3);
	outtextxy(center_x + secondlength, center_y - 5, s_3);

	solidcircle(center_x - secondlength - 17, center_y, 4);
	TCHAR s_9[5];
	_stprintf(s_9, _T("%d"), 9);
	outtextxy(center_x - secondlength - 8, center_y - 5, s_9);


	int R = secondlength + 20;
	int x_11, y_11;
	x_11 = Width / 2 - R / 2;
	y_11 = High / 2 - sqrt(3.0) / 2 * R;
	solidcircle(x_11, y_11, 3);
	TCHAR s_11[5];
	_stprintf(s_11, _T("%d"), 11); 
	outtextxy(x_11, y_11, s_11);

	int x_10, y_10;
	x_10 = Width / 2 - sqrt(3.0) / 2 * R;
	y_10 = High / 2 - R / 2;
	solidcircle(x_10, y_10, 3);
	TCHAR s_10[5];
	_stprintf(s_10, _T("%d"), 10);
	outtextxy(x_10, y_10, s_10);

	int x_8, y_8;
	x_8 = x_10;
	y_8 = y_10 + R;
	solidcircle(x_8, y_8, 3);
	TCHAR s_8[5];
	_stprintf(s_8, _T("%d"), 8);
	outtextxy(x_8, y_8, s_8);

	int x_7, y_7;
	x_7 = x_11;
	y_7 = y_11 + sqrt(3.0) * R;
	solidcircle(x_7, y_7, 3);
	TCHAR s_7[5];
	_stprintf(s_7, _T("%d"), 7);
	outtextxy(x_7, y_7, s_7);

	int x_5, y_5;
	x_5 = x_7 + R;
	y_5 = y_7;
	solidcircle(x_5, y_5, 3);
	TCHAR s_5[5];
	_stprintf(s_5, _T("%d"), 5); 
	outtextxy(x_5, y_5, s_5);

	int x_4, y_4;
	x_4 = x_8 + sqrt(3.0) * R;
	y_4 = y_8;
	solidcircle(x_4, y_4, 3);
	TCHAR s_4[5];
	_stprintf(s_4, _T("%d"), 4);
	outtextxy(x_4, y_4, s_4);

	int x_2, y_2;
	x_2 = x_10 + sqrt(3.0) * R;
	y_2 = y_10;
	solidcircle(x_2, y_2, 3);
	TCHAR s_2[5];
	_stprintf(s_2, _T("%d"), 2);
	outtextxy(x_2, y_2, s_2);

	int x_1, y_1;
	x_1 = x_11 + R;
	y_1 = y_11;
	solidcircle(x_1, y_1, 3);
	TCHAR s_1[5];
	_stprintf(s_1, _T("%d"), 1);
	outtextxy(x_1, y_1, s_1);

	float secondangle = 0;
	float minuteangle = 0;
	float hourAngle = 0;
	SYSTEMTIME ti;
	BeginBatchDraw();
	while (1)
	{
		GetLocalTime(&ti);

		secondangle = ti.wSecond * 2 * PI / 60;
		secondend_x = center_x + secondlength * sin(secondangle);
		secondend_y = center_y - secondlength * cos(secondangle);


		minuteangle = ti.wMinute * 2 * PI / 60;
		minuteend_x = center_x + minutelength * sin(minuteangle);
		minuteend_y = center_y - minutelength * cos(minuteangle);

		hourAngle = ti.wHour * 2 * PI / 12;
		hourEnd_x = center_x + hourlength * sin(hourAngle);
		hourEnd_y = center_y - hourlength * cos(hourAngle);

		setlinestyle(PS_SOLID, 2);
		setcolor(BLUE);
		line(center_x, center_y, secondend_x, secondend_y);

		setlinestyle(PS_SOLID, 2);
		setcolor(YELLOW);
		line(center_x, center_y, minuteend_x, minuteend_y);

		setlinestyle(PS_SOLID, 3);
		setcolor(RED);
		line(center_x, center_y, hourEnd_x, hourEnd_y);
		setcolor(GREEN);
		TCHAR s[] = _T("����������");
		outtextxy(x_11 + R / 3 - 10, y_11 + sqrt(3.0) / 2 * R + R / 2, s);
		FlushBatchDraw();
		Sleep(50);

		setcolor(BLACK);
		line(center_x, center_y, secondend_x, secondend_y);

	
		setcolor(BLACK);
		line(center_x, center_y, minuteend_x, minuteend_y);

		setcolor(BLACK);
		line(center_x, center_y, hourEnd_x, hourEnd_y);
	}

	EndBatchDraw();

	getch();
	closegraph();
	system("pause");
	return 0;
}
