#include <conio.h>
#include <graphics.h>
#include<windows.h>
#define High 480  // 游戏画面尺寸
#define Width 640
// 全局变量
int ball_x,ball_y; // 小球的坐标
int ball_vx,ball_vy; // 小球的速度
int radius; // 小球的半径
int bar1_left,bar1_right,bar1_top,bar1_bottom; // 挡板1的上下左右位置坐标
int bar2_left,bar2_right,bar2_top,bar2_bottom; // 挡板2的上下左右位置坐标
int bar_height,bar_width; // 挡板的高度、宽度
