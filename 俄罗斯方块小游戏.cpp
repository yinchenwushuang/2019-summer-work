#include<graphics.h>
#include <conio.h>
#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<windows.h>
#pragma comment (lib,"Winmm.lib")
#define WIDTH 200//游戏区宽度
#define HEIGHT 400//高度
#define UNIT 20//每个游戏区单位的实际像素

//全局变量
int g_arrBackGround[20][10]={0};//背景分割
int g_arrSqare[2][4]={0};
int n;
int g_nSqareID;
MOUSEMSG msg;
COLORREF c;//方块颜色
int g_nLine,g_nList;
int a;
int Score=0;
char strScore[10];
IMAGE img_bk1;//定义IMAGE对象
IMAGE img_bk2;


//函数声明
void gotoxy(int x,int y);//清屏
void startup();//初始化
void show();//显示函数，清全屏
void UpdateWithoutInput();//与用户无关的输入
void UpdateWithInput();//与用户有关的输入
void CreateRandonSqare();//随机显示图形
void CopySqareToBack();//把图形写入背景数组
void SqareDown();//下降
void SqareLeft();//左移
void SqareRight();//右移
void OnChangeSqare();//变形
void ChangeSqare();//除长条和正方形外的变形
void ChangeLineSqare();//长条变形
int CanSqareChangeShape();//解决变形bug
int CanLineSqareChange();//解决长条变形bug
int gameover();//判断游戏是否失败
int CanSqareDown();//若返回0继续下降，返回1则代表到底，不下降
int CanSqareDown2();//若返回0继续下降，返回1则代表到底，不下降,与方块相遇
int CanSqareLeft();//若返回0继续左移，返回1则代表到最左边，不再左移
int CanSqareLeft2();//若返回0继续左移，返回1则代表到最左边，不再左移，与方块相遇
int CanSqareRight();//若返回0继续右移，返回1则代表到最右边，不再右移
int CanSqareRight2();//若返回0继续右移，返回1则代表到最右边，不再右移，与方块相遇
void PaintSqare();//画方块
void Change1TO2();//到底之后数组由1变2
void ShowSqare2();//2的时候也画方块到背景
void DestroyOneLineSqare();//消行

int CanSqareChangeShape()
{
	int i, j;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (g_arrBackGround[g_nLine + i][g_nList + j] == 2)
			{
				return 0;
			}
		}
	}
	if (g_nList < 0)
	{
		g_nList = 0;
	}
	else if (g_nList + 2 > 9)
	{
		g_nList = 7;
	}
	return 1;
}
//清屏
void gotoxy(int x,int y)
{

	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);

	COORD pos;

	pos.X=x;

	pos.Y=y;

	SetConsoleCursorPosition(handle,pos);
}

