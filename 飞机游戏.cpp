#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>
void show();
void startup( );
void updateWithoutInput();
void updateWithInput();
void gotoxy(int x,int y);
void HideCursor();


int position_x, position_y;
int bullet_x, bullet_y;
int enemy_x, enemy_y;
int high, width,score,n=0,m=0;

int main(void )
{
	startup();      //初始化
	HideCursor();
	while(1)       //游戏循环执行
	{
	show();                  //显示画面
	updateWithoutInput();   //与用户输人无关的更新
	updateWithInput();     //与用户输人有关的更新
	}
	
	return 0;
} 

void startup()
{
	high = 20;
	width = 30;
	position_x = high/2;
	position_y =width/2;
	bullet_x = -2;
	bullet_y = position_y;
	enemy_x = 0;
	enemy_y = position_y;
	score = 0;
}

void show()
{
	gotoxy(0,0);
	int i,j;
	for(i=0;i<high;i++)
	{
		for (j=0;j<width;j++)
		{
			if ((i==position_x)&&(j==position_y))
			    printf("*");
			else if((i==enemy_x)&&(j==enemy_y))
			    printf("@");
			else if((i==bullet_x)&&(j==bullet_y))
			    printf("+");
			else
			    printf(" ");
		}
		printf("\n" );
    }
    printf("得分：%d\n",score );
}


void updateWithInput()
{
	char input; 
	if(kbhit())
	{
		input=getch();
		if (input == 'a')
		    position_y-- ;
		if (input == 'd')
		    position_y++ ;
		if (input == 'w')
		    position_x-- ;
		if (input == 's')
		    position_x++ ;
		if (input == ' ')
		{
			bullet_x = position_x - 1;
			bullet_y = position_y;
		}
	}
}

void updateWithoutInput()
{
	if (bullet_x>-1)
	    bullet_x--;
	if ((bullet_x == enemy_x) && (bullet_y == enemy_y))
	{
		score++;
		enemy_x = rand() % width;
		enemy_y = rand() % width;
		bullet_x = -2;
	}

	if (enemy_x > high)
	{
		enemy_x = 1;
		enemy_y = rand() % width;
	}
	static int speed = 0;
	if(speed<10)
	    speed++;
	if(speed == 10)
	{
		if((enemy_y<=width)&&(enemy_y!=0)&&n==0)
		{
			enemy_y--;
		}
		    
		else
		{
			enemy_y++;
		    n=1;
		    if(enemy_y==width)
		    n=0;  
		}
		if(enemy_y>width||enemy_y<0)
		    enemy_y++;
		speed = 0;
	}
	 
}

void gotoxy(int x,int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos);
}

void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor_info={1,0};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info);
}




























